from hmmlearn import hmm
import numpy as np
import math

states = ['d1', 'd2',"end"]
hidde_states = ('1', '2', '3') #just to help to give the sequenced

model = hmm.MultinomialHMM(n_components=3,algorithm='viterbi')
model.startprob_ = np.array([0.5,0.5,0])
model.transmat_ = np.array([[0.5,0.25,0.25],
			    [0.25,0.5,0.25],
                            [1, 0,0]])
model.emissionprob_ = np.array([[0.5, 0.25, 0.25],
                                [0.5, 0.25, 0.25],
				[0.5, 0.25, 0.25]])
logprob, seq = model.decode(np.array([[0,0,1,0,1,1]]).transpose())
print("The logarithmic propability is:",math.exp(logprob))
print("The most likely states for sequence 112122 is:")

for i in seq:
   print(states[seq[i]])

