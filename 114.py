from hmmlearn import hmm
import numpy as np
import math

states = ['a', 'b']
hiddeν_states = ('A', 'G', 'T','C') #just to help to give the sequenced

model = hmm.MultinomialHMM(n_components=2,algorithm='viterbi')
model.startprob_ = np.array([0.5, 0.5]) #start propabilities
model.transmat_ = np.array([[0.9,0.1],		#tranasmition propabilities
                            [0.1, 0.9]])
model.emissionprob_ = np.array([[0.4, 0.4, 0.1, 0.1],
                                [0.2, 0.2, 0.3, 0.3]]) #emission propabilities 
logprob, seq = model.decode(np.array([[2,1,3,2]]).transpose())  #creation of the best sequence and calculation of log propabillity
print("The logarithmic propability is:",math.exp(logprob))
print("The most likely states for sequence GGCT is:")

for i in seq:
   print(states[seq[i]])

